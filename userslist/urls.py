from django.conf.urls import patterns, include, url
from django.contrib import admin

from users import views

urlpatterns = patterns(
    '',
    # Examples:
    # url(r'^$', 'userslist.views.home', name='home'),
    url(r'^$', views.userlist, name='userlist'),
    url(r'^users/', include('users.urls')),

    url(r'^admin/', include(admin.site.urls)),
)
