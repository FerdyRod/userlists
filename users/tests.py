from django.test import TestCase
from django.test.client import Client


# Create your tests here.
class AddViewTestCase(TestCase):

    def test_home(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertTrue('users' in response.context)
