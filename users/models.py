import random

from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class UserProfile(models.Model):
    user = models.OneToOneField(User, unique=True)
    birthday = models.DateField()
    random_number = models.PositiveIntegerField()

    def get_random_number(value):
        return random.randrange(1, 101)

    def save(self, *args, **kwargs):
        number = self.get_random_number()
        if not self.random_number:
            self.random_number = number
        super(UserProfile, self).save(*args, **kwargs)
