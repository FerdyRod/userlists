from django import template
from datetime import date

register = template.Library()


def calculate_age(dob):
    today = date.today()
    return today.year - dob.year - ((today.month, today.day) < (dob.month, dob.day))


@register.simple_tag(takes_context=True)
def is_eligible(context, birthday_date):
    age = calculate_age(birthday_date)
    if age > 13:
        return "allowed"
    return "blocked"


def calculate_bizzfuzz(n):
    if n % 3 == 0:
        return "Bizz"
    elif n % 5 == 0:
        return "Fuzz"
    elif n % 3 != 0 and n % 5 != 0:
        return "BizzFuzz"
    return "Error"


@register.simple_tag(takes_context=True)
def bizzfuzz(context, number):
    return calculate_bizzfuzz(number)
