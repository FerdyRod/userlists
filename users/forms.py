import floppyforms.__future__ as forms
from django.contrib.auth.models import User

from models import UserProfile


class UserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = User
        fields = ('username', 'password')


class DatePicker(forms.DateInput):
    template_name = 'datepicker.html'

    class Media:
        js = (
            'js/jquery.min.js',
            'js/jquery-ui.min.js',
        )
        css = {
            'all': (
                'css/jquery-ui.css',
            )
        }


class UserProfileForm(forms.ModelForm):
    birthday = forms.DateField(widget=DatePicker)

    class Meta:
        model = UserProfile
        fields = ('birthday', )
