from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from models import UserProfile


# Adding the userprofile fields to the User Admin site, for easy testing.

class UserProfileInline(admin.StackedInline):
    '''
        Stacked Inline View for UserProfile
    '''
    model = UserProfile
    fields = ('birthday', )


class UserAdmin(UserAdmin):
    inlines = (UserProfileInline, )

admin.site.unregister(User)
admin.site.register(User, UserAdmin)
