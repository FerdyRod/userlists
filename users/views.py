from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponseBadRequest
from django.contrib.auth.models import User

from forms import UserForm, UserProfileForm


# Create your views here.
def userlist(request):
    # We filter out the superusers to prevent deletions of those users
    users = User.objects.filter(is_superuser=False)
    return render(request, 'userlist.html', {'users': users})


def add(request):
    if request.method == 'POST':
        user_form = UserForm(request.POST)
        userprofile_form = UserProfileForm(request.POST)
        if user_form.is_valid() and userprofile_form.is_valid():
            user = user_form.save(commit=False)
            user.save()
            userprofile = userprofile_form.save(commit=False)
            userprofile.user = user
            userprofile.save()
            return redirect('/')
        else:
            return HttpResponseBadRequest('There was a problem procesing your request, Please try again')
    else:
        user_form = UserForm()
        userprofile_form = UserProfileForm()
    return render(request, 'add.html', {'user_form': user_form, 'userprofile_form': userprofile_form})


def edit(request, user_id):
    user = get_object_or_404(User, pk=user_id)
    if user:
        if request.method == 'POST':
            userprofile_form = UserProfileForm(request.POST, instance=user)
            new_birthday = request.POST['birthday']
            if userprofile_form.is_valid():
                userprofile_form = userprofile_form.save(commit=False)
                user.userprofile.birthday = new_birthday
                user.userprofile.save()
                return redirect('/')
            else:
                return HttpResponseBadRequest('There was a problem procesing your request, Please try again')
        else:
            userprofile_form = UserProfileForm(instance=user.userprofile)
    return render(request, 'edit.html', {'userprofile_form': userprofile_form, 'user': user})


def delete(request, user_id):
    user = get_object_or_404(User, pk=user_id)
    user.delete()
    return redirect('/')
