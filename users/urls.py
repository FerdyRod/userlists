from django.conf.urls import patterns, url

import views

urlpatterns = patterns(
    '',
    url(r'^add/$', views.add, name='add'),
    url(r'^edit/(?P<user_id>.*)$', views.edit, name='edit'),
    url(r'^delete/(?P<user_id>.*)$', views.delete, name='delete'),
)
